from django.urls import path
from improvement_plan import views

from rest_framework import routers
from improvement_plan import viewsets

app_name = 'improvement_plan'

router = routers.SimpleRouter()

# **************************************
#               API REST
# **************************************
router.register('faculties', viewsets.FacultyViewSet)
router.register('programs', viewsets.ProgramViewSet)
router.register('plans', viewsets.ImprovementPlanViewSet)
router.register('objectives', viewsets.ObjectiveViewSet)
router.register('indicators', viewsets.IndicatorViewSet)
router.register('goals', viewsets.GoalViewSet)
router.register('periodicities', viewsets.PeriodicityViewSet)
router.register('tendencies', viewsets.TendencyViewSet)
router.register('measurement', viewsets.MeasurementViewSet)
router.register('users', viewsets.UserViewSet)
router.register('groups', viewsets.GroupViewSet)
router.register('permissions', viewsets.PermissionViewSet)
router.register('variables', viewsets.VariableViewSet)
router.register('calculated', viewsets.CalculatedViewSet)
router.register('type_measurement', viewsets.TypeMeasurementViewSet)
router.register('activities', viewsets.ActivityViewSet)
router.register('states', viewsets.StateViewSet)

urlpatterns = router.urls
