from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission

from improvement_plan import models


class FacultySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Faculty
        fields = '__all__'


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Program
        fields = '__all__'


class ImprovementPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ImprovementPlan
        fields = '__all__'


class ObjectiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Objective
        fields = '__all__'


class IndicatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Indicator
        fields = '__all__'


class GoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Goal
        fields = '__all__'


class PeriodicitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Periodicity
        fields = '__all__'


class TendencySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tendency
        fields = '__all__'


class MeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Measurement
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ('id', 'username', 'email', 'password', 'groups')
        

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group
        fields = '__all__'


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Permission
        fields = '__all__'        

class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Variable
        fields = '__all__'


class CalculatedSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Calculated
        fields = '__all__'


class TypeMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TypeMeasurement
        fields = '__all__'


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Activity
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.State
        fields = '__all__'
