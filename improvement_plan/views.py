from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

import json
import jwt

def dataToken(request):
    print("request: ", request.POST)
    
    decodedPayload = jwt.decode('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImRpZWdvejQiLCJleHAiOjE1NTIwMjEyMDMsImVtYWlsIjoiIn0.rv6DUPoo9bJ29c0fYKq8whr0nrHA6-G2AOqIhR5nnuQ', None, None)
    # 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImRpZWdvejQiLCJleHAiOjE1NTIwMjEyMDMsImVtYWlsIjoiIn0.rv6DUPoo9bJ29c0fYKq8whr0nrHA6-G2AOqIhR5nnuQ'

    # print(decodedPayload)
    return HttpResponse(json.dumps(decodedPayload), content_type='application/json')

