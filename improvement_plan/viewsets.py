from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from improvement_plan import models
from improvement_plan import serializer


class FacultyViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)

    queryset = models.Faculty.objects.all()
    serializer_class = serializer.FacultySerializer


class ProgramViewSet(viewsets.ModelViewSet):
    queryset = models.Program.objects.all()
    serializer_class = serializer.ProgramSerializer


class ImprovementPlanViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)

    queryset = models.ImprovementPlan.objects.all()
    serializer_class = serializer.ImprovementPlanSerializer


class ObjectiveViewSet(viewsets.ModelViewSet):
    queryset = models.Objective.objects.all()
    serializer_class = serializer.ObjectiveSerializer


class IndicatorViewSet(viewsets.ModelViewSet):
    queryset = models.Indicator.objects.all()
    serializer_class = serializer.IndicatorSerializer


class GoalViewSet(viewsets.ModelViewSet):
    queryset = models.Goal.objects.all()
    serializer_class = serializer.GoalSerializer


class PeriodicityViewSet(viewsets.ModelViewSet):
    queryset = models.Periodicity.objects.all()
    serializer_class = serializer.PeriodicitySerializer


class TendencyViewSet(viewsets.ModelViewSet):
    queryset = models.Tendency.objects.all()
    serializer_class = serializer.TendencySerializer


class MeasurementViewSet(viewsets.ModelViewSet):
    queryset = models.Measurement.objects.all()
    serializer_class = serializer.MeasurementSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = models.User.objects.all().order_by('id')
    serializer_class = serializer.UserSerializer

class GroupViewSet(viewsets.ModelViewSet):
    queryset = models.Group.objects.all().order_by('id')
    serializer_class = serializer.GroupSerializer

class PermissionViewSet(viewsets.ModelViewSet):
    queryset = models.Permission.objects.all().order_by('id')
    serializer_class = serializer.PermissionSerializer

class VariableViewSet(viewsets.ModelViewSet):
    queryset = models.Variable.objects.all().order_by('id')
    serializer_class = serializer.VariableSerializer


class CalculatedViewSet(viewsets.ModelViewSet):
    queryset = models.Calculated.objects.all()
    serializer_class = serializer.CalculatedSerializer


class TypeMeasurementViewSet(viewsets.ModelViewSet):
    queryset = models.TypeMeasurement.objects.all()
    serializer_class = serializer.TypeMeasurementSerializer


class ActivityViewSet(viewsets.ModelViewSet):
    queryset = models.Activity.objects.all()
    serializer_class = serializer.ActivitySerializer


class StateViewSet(viewsets.ModelViewSet):
    queryset = models.State.objects.all()
    serializer_class = serializer.StateSerializer