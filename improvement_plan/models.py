from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission

from django.utils import timezone


class Faculty(models.Model):
    name = models.CharField(blank=True, max_length=500)

    def __str__(self):
        return self.name


class Program(models.Model):
    name = models.CharField(blank=True, max_length=500)
    faculty = models.ForeignKey('Faculty', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ImprovementPlan(models.Model):
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    program = models.ForeignKey('Program', on_delete=models.CASCADE)

    name = models.CharField(blank=True, max_length=500)
    description = models.CharField(blank=True, max_length=500)
    creation_date = models.DateField(blank=True, auto_now_add=True)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)

    def __str__(self):
        return self.name


class Objective(models.Model):
    improvement_plan = models.ForeignKey(
        'ImprovementPlan', on_delete=models.CASCADE)
    autor = models.ForeignKey(User, on_delete=models.CASCADE)

    name = models.CharField(blank=True, max_length=500)
    observations = models.CharField(blank=True, max_length=500)
    creation_date = models.DateField(blank=True, auto_now_add=True)

    def __str__(self):
        return self.name


class Indicator(models.Model):
    objective = models.ForeignKey('Objective', on_delete=models.CASCADE)
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    periodicity = models.ForeignKey('Periodicity', on_delete=models.CASCADE)
    tendency = models.ForeignKey('Tendency', on_delete=models.CASCADE)
    typeMeasurement = models.ForeignKey('TypeMeasurement', on_delete=models.CASCADE)

    responsable = models.CharField(blank=True, max_length=150)
    name = models.CharField(blank=True, max_length=500)
    observations = models.CharField(blank=True, max_length=750)
    formula = models.CharField(blank=True, max_length=100)
    description_formula = models.CharField(blank=True, max_length=500)
    base_line = models.CharField(blank=True, max_length=10)
    creation_date = models.DateField(blank=True, auto_now_add=True)

    def __str__(self):
        return self.name


class Goal(models.Model):
    indicator = models.ForeignKey('Indicator', on_delete=models.CASCADE)

    goal_date = models.IntegerField(blank=True)
    value = models.CharField(blank=True, max_length=10)
    responsable = models.CharField(blank=True, max_length=100)

    def __str__(self):
        return self.value


class Periodicity(models.Model):
    name = models.CharField(blank=True, max_length=50)
    months = models.IntegerField(default=2)

    def __str__(self):
        return self.name


class Tendency(models.Model):
    name = models.CharField(blank=True, max_length=5)

    def __str__(self):
        return self.name

# new classes

class Variable(models.Model):
    indicator = models.ForeignKey('Indicator', on_delete=models.CASCADE)

    variable_name = models.CharField(blank=True, max_length=50)
    variable = models.CharField(blank=True, max_length=2)

    def __str__(self):
        return self.variable_name


class Measurement(models.Model):
    indicator = models.ForeignKey('Indicator', on_delete=models.CASCADE)
    variable = models.ForeignKey('Variable', on_delete=models.CASCADE)

    name = models.CharField(blank=True, max_length=100)
    value = models.CharField(blank=True, max_length=10)
    responsable = models.CharField(blank=True, max_length=100)
    measurement_date = models.DateField(blank=True, max_length=10)
    description = models.CharField(blank=True, max_length=500)

    def __str__(self):
        return self.value


class Calculated(models.Model):
    indicator = models.ForeignKey('Indicator', on_delete=models.CASCADE)

    date_calculated = models.DateField(blank=True)
    value = models.CharField(blank=True, max_length=10)

    def __str__(self):
        return self.value


class TypeMeasurement(models.Model):
    name = models.CharField(blank=True, max_length=50)

    def __str__(self):
        return self.name

# Actividades


class Activity(models.Model):
    indicator = models.ForeignKey('Indicator', on_delete=models.CASCADE)
    state = models.ForeignKey('State', on_delete=models.CASCADE)

    name = models.CharField(blank=True, max_length=500)
    responsable = models.CharField(blank=True, max_length=150)
    estimated = models.CharField(blank=True, max_length=500)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)

    def __str__(self):
        return self.name

class State(models.Model):
    state = models.CharField(blank=True, max_length=10)

    def __str__(self):
        return self.state