from django.apps import AppConfig


class ImprovementPlanConfig(AppConfig):
    name = 'improvement_plan'
