from django.contrib import admin
from improvement_plan import models

from django.contrib.auth.models import Permission

admin.site.register(models.Faculty)
admin.site.register(models.Program)
admin.site.register(models.ImprovementPlan)
admin.site.register(models.Objective)
admin.site.register(models.Indicator)
admin.site.register(models.Goal)
admin.site.register(models.Periodicity)
admin.site.register(models.Tendency)
admin.site.register(models.Variable)
admin.site.register(models.Measurement)
admin.site.register(models.Calculated)
admin.site.register(models.TypeMeasurement)
admin.site.register(models.Activity)
admin.site.register(models.State)
admin.site.register(Permission)
